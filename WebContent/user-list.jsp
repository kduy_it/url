<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title></title>
	<style type="text/css">
		table {
			width: 80%;
			margin: 10px 50px 10px 20px;
			text-align: center;
		}
		#toRegister {
		    width: 100px;
		    float: right;
    		margin-right: 19%;
    		cursor: pointer;
		}
		.insideBtn {
			margin: 2px 0px 2px 2px;
	    	width: 48%;
	    	cursor: pointer;
		}
		thead {
			background: lightgrey;
		}
	</style>
</head>
<body>
    <form>
		<a href="new">
			<input type="button" id="toRegister" value="新規作成" />
		</a>
		<br>
		<table border="1">
			<thead>
	    		<tr>
	        		<td>ID</td>
	        		<td>名前</td>
	        		<td>生年月日</td>
	        		<td>部門名</td>
	        		<td></td>
	    		</tr>
	    	</thead>
	    	<c:forEach var="user" items="${listUser}">
	    		<tr>
	    			<td><c:out value="${user.userID}" /></td>
	    			<td><c:out value="${user.userName}" /></td>
	    			<td><c:out value="${user.birthday}"></c:out></td>
	    			<td><c:out value="${user.departmentName}"></c:out></td>
					<td>
						<a href="edit?id=<c:out value='${user.userID}'></c:out>">
							<input class="insideBtn" type="button" value="便新"/>
						</a>
						<a href="delete?id=<c:out value='${user.userID}'></c:out>">
							<input class="insideBtn" type="button" value="削除" onclick="return confirm('ID: ${user.userID} ${user.userName} を削除してもよろしいですか。')"/>
						</a>
					</td>
	    		</tr>
	    	</c:forEach>
		</table>
	</form>
</body>
</html>