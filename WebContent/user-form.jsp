<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script>
	function validLogin(){
	if (document.form.userName.value == ""){
	alert ( "Please enter Login Name." );
	document.loginform.userName.focus();
	return false;
	}
	if (document.form.password.value == ""){
	alert ( "Please enter password." );
	document.userform.password.focus();
	return false;
	}
	alert ( "Welcome User" );
	return true;
	}
	</script>
<title>Insert title here</title>
<style>
	body {
		font-size: 20px;
	}
	table {
		margin: 50px 0px 0px 20px;
		width: 90%;
		border: 0;
		border-spacing: 20px;
	}
	#back {
		margin: 10px 0px 0px 10px;
		width: 8%;
		cursor: pointer;
	}
	#register {
		margin: 10px 10px 0px 10px;
		float: right;
		width: 8%;
		cursor: pointer;
	}
	#memo {
		position: absolute;
	}
</style>
</head>
<body>
        <c:if test="${user != null}">
			<form action="update" method="post">
			<input id="register" type="submit" value="便新"/>
        </c:if>
        <c:if test="${user == null}">
			<form action="insert" method="post">
			<input id="register" type="submit" value="登録"/>
        </c:if>
        <form action="register" method="post">
		<a href="list">
				<input type="button" id="back" value="戻る" />
		</a>
		<table>
			<c:if test="${user != null}">
				<input type="hidden" name="id" value="<c:out value='${user.id}' />"></input>
			</c:if>
			<tr>
	        	<td>名前</td>
	        	<td> <input type="text" name="name" value="<c:out value='${user.name}' />" /></td>
	    	</tr>
	    	<tr>
	        	<td>生年月日</td>
	        	<td> <input type="date" name="birthday" value="<c:out value='${user.birthday}' />"></td>
	    	</tr>
	    	<tr>
	        	<td>専門</td>
	        	<td>
					<select name="departmentID">
						<option selected hidden="true"></option>
						<c:forEach items="${listDepartment}" var="department">
							<option value="${department.id}" ${(user eq null && param.departmentID eq department.id) || (user != null && user.departmentId eq department.id)
							 ? 'selected' : ''}> ${department.name} </option>
						</c:forEach>
					</select>
				</td>
	    	</tr>
	    	<tr>
	        	<td id="memo">メモ</td>
	        	<td>
	        		<textarea name="memo" rows="6" cols="100"> <c:out value="${user.memo}"></c:out></textarea>
	        	</td>
	    	</tr>
		</table>
	</form>
</body>
</html>