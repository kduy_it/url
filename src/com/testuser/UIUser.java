package com.testuser;

public class UIUser {
	private int userID;
	private String userName;
	private String birthday;
	private int departmentID;
	private String departmentName;
	private String memo;

	public UIUser(int userID, String userName, String birthday, int departmentID, String departmentName, String memo) {
		this.userID = userID;
		this.setUserName(userName);
		this.setBirthday(birthday);
		this.departmentID = departmentID;
		this.setDepartmentName(departmentName);
		this.setMemo(memo);
	}

	public int getUserID() {
		return userID;
	}

	public int getDepartmentID() {
		return departmentID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
}
