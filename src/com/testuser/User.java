package com.testuser;

public class User {
	private int id;
	private String name;
	private String birthday;
	private int departmentId;

	private String memo;

	public User(String name, String birthday, int department_id, String memo) {
		this.setName(name);
		this.setBirthday(birthday);
		this.setDepartmentId(department_id);
		this.setMemo(memo);
	}

	public User(int id, String name, String birthday, int department_id, String memo) {
		this.id = id;
		this.setName(name);
		this.setBirthday(birthday);
		this.setDepartmentId(department_id);
		this.setMemo(memo);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
}
