package com.testuser;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class UserDAO {
	private String jdbcURL = "jdbc:mysql://localhost:3306/user?useSSL=false&useUnicode=true&characterEncoding=UTF-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private String jdbcUsername = "root";
	private String jdbcPassword = "pa$$9680";

	private String dbTimeFormat = "yyyy-MM-dd";
	private String jpTimeFormat = "yyyy年M月d日";
	private static final String INSERT_USERS_SQL = "INSERT INTO user_master" + "  (name, birthday, department_id, memo) VALUES "
			+ " (?, ?, ?, ?);";

	private static final String SELECT_USER_BY_ID = "select * from user_master where id = ?";
	private static final String SELECT_ALL_USERS = "select * from user_master order by id";
	private static final String SELECT_ALL_USERS_AND_DEPARTMENT_NAME = "select a.id, a.name, a.birthday, a.department_id, b.name as department_name, a.memo from user_master as a, department_master as b where a.department_id = b.id order by a.id";
	private static final String DELETE_USERS_SQL = "delete from user_master where id = ?;";
	private static final String UPDATE_USERS_SQL = "update user_master set name = ?,birthday = ?, department_id = ?, memo = ? where id = ?;";

	public UserDAO() {
	}

	protected String jpTimeConvert(String time) throws ParseException {
		Locale locale = new Locale("ja", "JP");
		Date parsedTime = new SimpleDateFormat(dbTimeFormat).parse(time);
	    DateFormat kanjiFormat = new SimpleDateFormat(jpTimeFormat, locale);
		return kanjiFormat.format(parsedTime);
	}
	protected Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public void insertUser(User user) throws SQLException {
		System.out.println(INSERT_USERS_SQL);
		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL)) {
			preparedStatement.setString(1, user.getName());
			preparedStatement.setString(2, user.getBirthday());
			preparedStatement.setInt(3, user.getDepartmentId());
			preparedStatement.setString(4, user.getMemo());
			System.out.println(preparedStatement);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			printSQLException(e);
		}
	}

	public User selectUser(int id) {
		User user = null;
		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID);) {
			preparedStatement.setInt(1, id);
			System.out.println(preparedStatement);
			ResultSet result = preparedStatement.executeQuery();

			while (result.next()) {
				String name = result.getString("name");
				String birthday = result.getString("birthday");
				int department_id = result.getInt("department_id");
				String memo = result.getString("memo");
				user = new User(id, name, birthday, department_id, memo);
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return user;
	}

	public List<User> selectAllUsers() {

		List<User> users = new ArrayList<>();
		try (Connection connection = getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS);) {
			System.out.println(preparedStatement);
			ResultSet result = preparedStatement.executeQuery();

			while (result.next()) {
				int id = result.getInt("id");
				String name = result.getString("name");
				String birthday = this.jpTimeConvert(result.getString("birthday"));
				int department_id = result.getInt("department_id");
				String memo = result.getString("memo");
				users.add(new User(id, name, birthday, department_id, memo));
			}
		} catch (SQLException e) {
			printSQLException(e);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return users;
	}

	public List<UIUser> selectUserForUI() {
		List<UIUser> userList = new ArrayList<>();
		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS_AND_DEPARTMENT_NAME);) {
			System.out.println(preparedStatement);
			ResultSet result = preparedStatement.executeQuery();

			while (result.next()) {
				int userID = result.getInt("id");
				String userName = result.getString("name");
				String birthday =this.jpTimeConvert(result.getString("birthday"));
				String memo = result.getString("memo");
				int departmentID = result.getInt("department_id");
				String departmentName = result.getString("department_name");
				userList.add(new UIUser(userID, userName, birthday, departmentID, departmentName, memo));
			}
		} catch (SQLException e) {
			printSQLException(e);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return userList;
	}
	public boolean deleteUser(int id) throws SQLException {
		boolean rowDeleted;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_USERS_SQL);) {
			statement.setInt(1, id);
			rowDeleted = statement.executeUpdate() > 0;
		}
		return rowDeleted;
	}

	public boolean updateUser(User user) throws SQLException {
		boolean rowUpdated;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(UPDATE_USERS_SQL);) {
			statement.setString(1, user.getName());
			statement.setString(2, user.getBirthday());
			statement.setInt(3, user.getDepartmentId());
			statement.setString(4, user.getMemo());
			statement.setInt(5, user.getId());

			rowUpdated = statement.executeUpdate() > 0;
		}
		return rowUpdated;
	}

	private void printSQLException(SQLException ex) {
		for (Throwable e : ex) {
			if (e instanceof SQLException) {
				e.printStackTrace(System.err);
				System.err.println("SQLState: " + ((SQLException) e).getSQLState());
				System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
				System.err.println("Message: " + e.getMessage());
				Throwable t = ex.getCause();
				while (t != null) {
					System.out.println("Cause: " + t);
					t = t.getCause();
				}
			}
		}
	}

}
