package com.testuser;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class Main {

  public static void main(String[] argv) throws Exception {
	Locale locale = new Locale("ja", "JP");
	Date time = new SimpleDateFormat("yyyy-MM-dd").parse("1992-11-24");
	System.out.println( new SimpleDateFormat("yyyy-MM-dd").format(time));
    DateFormat kanjiFormat = new SimpleDateFormat("yyyy年MM月dd日", locale);
    System.out.println(kanjiFormat.format(time));
    String driver = "com.mysql.cj.jdbc.Driver";
    String connectionURL = "jdbc:mysql://localhost:3306/user?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    String user = "root";
    String password = "pa$$9680";
//    String SELECT_ALL_USER = "SELECT * FROM user_master";
//    String SELECT_ALL_USERS_AND_DEPARTMENT_NAME = "select a.id, a.name, a.birthday, a.department_id, b.name as department_name, a.memo from user_master as a, department_master as b where a.department_id = b.id";
    String UPDATE_USERS_SQL = "update user_master set name = ?,birthday = ?, department_id = ?, memo = ? where id = ?;";
    Class.forName(driver);
    Connection connection = DriverManager.getConnection(connectionURL, user, password);
    PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USERS_SQL);

//    ResultSet result = preparedStatement.executeQuery();
//    ArrayList <UIUser> userList = new ArrayList <UIUser> ();
//	while(result.next()) {
//		int userID = result.getInt("id");
//		String userName = result.getString("name");
//		String birthday = result.getString("birthday");
//		String memo = result.getString("memo");
//		int departmentID = result.getInt("department_id");
//		String departmentName = result.getString("department_name");
//		userList.add(new UIUser(userID, userName, birthday, departmentID, departmentName, memo));
//		System.out.println(departmentName);
//	}
    System.out.println("河合");
	User user1 = new User(4, "CCCasdads", "1999/7/21", 5, "Test");
	preparedStatement.setInt(5, user1.getId());
	preparedStatement.setString(1, user1.getName());
	preparedStatement.setString(2, user1.getBirthday());
	preparedStatement.setInt(3, user1.getDepartmentId());
	preparedStatement.setString(4, user1.getMemo());

	preparedStatement.executeUpdate();
    if (!connection.isClosed()) {
    	connection.close();
    }
  }
}