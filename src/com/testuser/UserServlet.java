package com.testuser;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/")
public class UserServlet extends HttpServlet {
	private UserDAO userDAO;
	private DepartmentDAO departmentDAO;

	public void init() {
		userDAO = new UserDAO();
		departmentDAO = new DepartmentDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getServletPath();

		try {
			switch (action) {
			case "/new":
				showNewForm(request, response);
				break;
			case "/insert":
				insertUser(request, response);
				break;
			case "/delete":
				deleteUser(request, response);
				break;
			case "/edit":
				showEditForm(request, response);
				break;
			case "/update":
				updateUser(request, response);
				break;
			default:
				listUser(request, response);
				break;
			}
		} catch (SQLException ex) {
			throw new ServletException(ex);
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	private void listUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException, ServletException {
//		List<User> listUser = userDAO.selectAllUsers();
		List<UIUser> listUser = userDAO.selectUserForUI();

		request.setAttribute("listUser", listUser);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("user-list.jsp");
		requestDispatcher.forward(request, response);
	}

	private void showNewForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, ClassNotFoundException {
		List<Department> listDepartment = departmentDAO.loadDepartmentList();

		request.setAttribute("listDepartment", listDepartment);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("user-form.jsp");
		requestDispatcher.forward(request, response);
	}

	private void showEditForm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException, ClassNotFoundException {
		int id = Integer.parseInt(request.getParameter("id"));
		User existingUser = userDAO.selectUser(id);
		List<Department> listDepartment = departmentDAO.loadDepartmentList();

		request.setAttribute("user", existingUser);
		request.setAttribute("listDepartment", listDepartment);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("user-form.jsp");
		requestDispatcher.forward(request, response);
	}

	private void insertUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException, NullPointerException {
		request.setCharacterEncoding("UTF-8");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		int departmentID = Integer.parseInt(request.getParameter("departmentID"));
		String memo = request.getParameter("memo");
		User newUser = new User(name, birthday, departmentID, memo);
		userDAO.insertUser(newUser);

		response.sendRedirect("list");
	}

	private void updateUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		request.setCharacterEncoding("UTF-8");
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		int departmentID = Integer.parseInt(request.getParameter("departmentID"));
		String memo = request.getParameter("memo");

		User updated = new User(id, name, birthday, departmentID, memo);
		userDAO.updateUser(updated);

		response.sendRedirect("list");
	}

	private void deleteUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));

		userDAO.deleteUser(id);

		response.sendRedirect("list");
	}

}
